'''
app.py starts up flask and creates views/routing for scalist-net
This file is the only directly flask-related file in this repo. 
'''
from flask import Flask, render_template

app = Flask(__name__)


@app.route('/')
def index():
    return render_template('index.html')


if __name__ == '__main__':
    app.run(host='0.0.0.0', port=8081)
