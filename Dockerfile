FROM python:3.6.7
ADD . /code
WORKDIR /code
RUN pip install pipenv
RUN pipenv install --system --deploy --ignore-pipfile
CMD ["python", "scalist.py"]
